'use strict';

/**
 *
 * @param {String} input input string
 * @returns true if input string is defined and valid, otherwise false
 */
function inputStringIsValid (input) {
  return input !== undefined && input.length > 0;
}

/**
 * Get string within delimiters.
 * @param {String} line input string
 * @param {Number} index zero-based position inside string
 * @param {String} openDel opening/first delimiter
 * @param {String} closeDel ending/last delimiter (optional)
 * @param {Boolean} withDel return string should have ALL input delimiter(s) (optional; default false)
 * @param {String} innerDel inner delimiter (optional)
 * @param {Boolean} withInnerDel return string should have ONLY INNER input delimiter (optional; default false)
 * @returns null if nothing was found or invalid input, otherwise a String
 */
function getStringWithinDelimiters (line, index, openDel, closeDel, withDel, innerDel, withInnerDel) {
  if (!(index >= 0 && index < line.length)) {
    return null; // nowhere to go!
  }

  const before = line.substring(0, index);
  const after = line.substring(index);

  const endChar = inputStringIsValid(closeDel) ? closeDel : openDel;

  let beforeIndex = before.lastIndexOf(openDel);
  let afterIndex = after.indexOf(endChar);

  if (afterIndex === -1) {
    if (beforeIndex === -1 || inputStringIsValid(closeDel)) {
      return null; // invalid input
    }

    afterIndex = after.length;
  }

  if (inputStringIsValid(closeDel)) {
    let innerSub = line.substring(beforeIndex, afterIndex + before.length - 1);
    while (innerSub.split(openDel).length - 1 !== innerSub.split(endChar).length) {
      const nextIndex = after.indexOf(endChar, afterIndex + endChar.length);
      if (nextIndex === -1) {
        break;
      }

      afterIndex = nextIndex;
      innerSub = line.substring(beforeIndex, afterIndex + before.length);
    }
  }

  !withDel ? beforeIndex += openDel.length : afterIndex += endChar.length;

  const out = before.substring(beforeIndex) + after.substring(0, afterIndex);

  if (inputStringIsValid(innerDel)) {
    const innerIndex = out.indexOf(innerDel);
    if (innerIndex === -1) {
      return null; // expected inner, but got invalid result
    }

    if (!(withInnerDel || withDel)) {
      return index <= beforeIndex + innerIndex ? out.substring(0, innerIndex) : out.substring(innerIndex + innerDel.length);
    }
  }

  return out;
}

/**
 * Get string within a surround delimiter.
 * @param {String} line input string
 * @param {Number} index zero-based position inside string
 * @param {String} del opening/closing delimiter
 * @param {Boolean} withDel return string should have ALL input delimiter(s) (optional; default false)
 * @param {String} innerDel inner delimiter (optional)
 * @param {Boolean} withInnerDel return string should have ONLY INNER input delimiter (optional; default false)
 * @returns null if nothing was found or invalid input, otherwise a String
 */
function getStringWithinSurroundDelimiter (line, index, del, withDel, innerDel, withInnerDel) {
  return getStringWithinDelimiters(line, index, del, undefined, withDel, innerDel, withInnerDel);
}

module.exports = { getStringWithinDelimiters, getStringWithinSurroundDelimiter };
