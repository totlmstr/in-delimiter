import { strictEqual } from 'assert';
import { describe, it } from 'mocha';

/* eslint-disable no-template-curly-in-string */

import { getStringWithinDelimiters as get, getStringWithinSurroundDelimiter as getInSingle } from '../index.js';

describe('One Delimiter', function () {
  it('Get Base Case', function () {
    strictEqual(getInSingle('Get Inner Word', 7, ' '), 'Inner');
  });

  it('Get Base Case WITH Delimiter', function () {
    strictEqual(getInSingle('Get Inner Word', 7, ' ', true), ' Inner ');
  });

  it('Get Base Case AT Delimiter', function () {
    strictEqual(getInSingle('Get Inner Word', 4, ' '), 'Inner');
  });

  it('Get FIRST String', function () {
    strictEqual(getInSingle('Get Inner Word', 2, ' '), 'Get');
  });

  it('Get LAST String', function () {
    strictEqual(getInSingle('Get Inner Word', 12, ' '), 'Word');
  });

  it('Get String (ERROR: bad input)', function () {
    strictEqual(getInSingle('', 4, ' '), null); // error
  });

  it('Get String (ERROR: cannot find delimiter)', function () {
    strictEqual(getInSingle('GetInnerWord', 6, ' '), null); // error
  });
});

describe('Open and Close Delimiters WITHOUT Inner Delimiter', function () {
  it('Get Base Case', function () {
    strictEqual(get('cmake_minimum_required(VERSION 3.1', 10, ' ', '('), 'cmake_minimum_required');
  });

  it('Get FIRST String', function () {
    strictEqual(get('${CMAKE_CURRENT_SOURCE_DIR} ${inner_${second_${sources}_deeper}_dirs}', 20, '${', '}'), 'CMAKE_CURRENT_SOURCE_DIR');
  });

  it('Get OUTER String', function () {
    strictEqual(get('${inner_${second_${sources}_deeper}_dirs}', 6, '${', '}'), 'inner_${second_${sources}_deeper}_dirs');
  });

  it('Get INNER String', function () {
    strictEqual(get('${inner_${second_${sources}_deeper}_dirs}', 15, '${', '}'), 'second_${sources}_deeper');
  });

  it('Get INNER String WITH Delimiters', function () {
    strictEqual(get('${inner_${second_${sources}_deeper}_dirs}', 15, '${', '}', true), '${second_${sources}_deeper}');
  });

  it('Get DEEPEST String', function () {
    strictEqual(get('${inner_${second_${sources}_deeper}_dirs}', 24, '${', '}'), 'sources');
  });

  it('Get DEEPEST String WITH ALL Delimiters', function () {
    strictEqual(get('${inner_${second_${sources}_deeper}_dirs}', 24, '${', '}', true), '${sources}');
  });

  it('Get String (ERROR: no closing)', function () {
    strictEqual(get('${CMAKE_CURRENT_SOURCE_DIR', 10, '${', '}'), null); // error
  });
});

describe('Open and Close Delimiters WITH INNER Delimiter', function () {
  it('Get LEFT Base Case', function () {
    strictEqual(get('$<INSTALL_DIRECTORY:include>', 15, '$<', '>', false, ':'), 'INSTALL_DIRECTORY');
  });

  it('Get RIGHT Base Case', function () {
    strictEqual(get('$<INSTALL_DIRECTORY:include>', 25, '$<', '>', false, ':'), 'include');
  });

  it('Get LEFT FIRST Inner String', function () {
    strictEqual(get('$<$<OR:$<CXX_COMPILER_ID:MSVC>>>', 4, '$<', '>', false, ':'), 'OR');
  });

  it('Get LEFT SECOND Inner String', function () {
    strictEqual(get('$<$<OR:$<CXX_COMPILER_ID:MSVC>>>', 15, '$<', '>', false, ':'), 'CXX_COMPILER_ID');
  });

  it('Get RIGHT SECOND Inner String', function () {
    strictEqual(get('$<$<OR:$<CXX_COMPILER_ID:MSVC>>>', 28, '$<', '>', false, ':'), 'MSVC');
  });

  it('Get SECOND Inner String WITH Delimiters', function () {
    strictEqual(get('$<$<OR:$<CXX_COMPILER_ID:MSVC>>>', 20, '$<', '>', true, ':'), '$<CXX_COMPILER_ID:MSVC>');
  });

  it('Get SECOND Inner String WITH INNER Delimiter', function () {
    strictEqual(get('$<$<OR:$<CXX_COMPILER_ID:MSVC>>>', 20, '$<', '>', false, ':', true), 'CXX_COMPILER_ID:MSVC');
  });

  it('Get RIGHT THIRD Inner String', function () {
    strictEqual(get('$<$<OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>>>', 53, '$<', '>', false, ':'), 'Clang');
  });

  it('Get THIRD Inner String WITH Delimiters', function () {
    strictEqual(get('$<$<OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>>>', 40, '$<', '>', true, ':'), '$<CXX_COMPILER_ID:Clang>');
  });

  it('Get THIRD Inner String WITH INNER Delimiter', function () {
    strictEqual(get('$<$<OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>>>', 40, '$<', '>', false, ':', true), 'CXX_COMPILER_ID:Clang');
  });

  it('Get FIRST Inner String WITH ALL Delimiters (worst case)', function () {
    strictEqual(get('$<$<OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>>>', 4, '$<', '>', true, ':'), '$<OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>>');
  });

  it('Get FIRST Inner String WITH INNER Delimiter (worst case)', function () {
    strictEqual(get('$<$<OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>>>', 4, '$<', '>', false, ':', true), 'OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>');
  });

  it('Get String (ERROR: no closing)', function () {
    strictEqual(get('$<INSTALL_DIRECTORY:include', 15, '$<', '>', false, ':'), null); // error
  });

  it('Get String (ERROR: no inner char)', function () {
    strictEqual(get('$<INSTALL_DIRECTORY include>', 15, '$<', '>', false, ':'), null); // error
  });
});
