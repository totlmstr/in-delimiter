# in-delimiter - Search in a String with One to Three Delimiters (No Regex)

- [in-delimiter - Search in a String with One to Three Delimiters (No Regex)](#in-delimiter---search-in-a-string-with-one-to-three-delimiters-no-regex)
  - [Features](#features)
  - [Overview](#overview)
  - [Case Examples and Usage](#case-examples-and-usage)
    - [Full Example: Different Outer Delimiters and Inner Delimiters](#full-example-different-outer-delimiters-and-inner-delimiters)
    - [Simplified Example: Same Outer Delimiter and Optional Inner Delimiter](#simplified-example-same-outer-delimiter-and-optional-inner-delimiter)
  - [Contributing](#contributing)
  - [License](#license)

## Features

- Zero Regular Expressions
- Zero dependencies
- Single, short, and simple function
- Up to three delimiters (begin, end, and inner) can be given, and can be returned as part of output
- Heavily tested (almost 30 cases)

## Overview

The primary motivation for this is [CMake](https://cmake.org/) syntax analysis (this can be seen in the test cases).

It's incredibly cumbersome to use Regex in a situation like this, especially in languages like CMake which use `$` or other reserved characters in Regex. Alternate ways like splitting the string will destroy the syntax and/or just leave you with garbage on top of garbage.

## Case Examples and Usage

### Full Example: Different Outer Delimiters and Inner Delimiters

Consider the following syntactically correct CMake snippet:

```cmake
if($<$<OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>>>)
# ...
endif()
```

Assume we want to get the `OR` segment with its associated delimiters. With this function, it is:

```js
let line = "if($<$<OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>>>)";
let result = getStringWithinDelimiters(line, line.indexOf("OR"), "$<", ">", true, ":");

// result now has "$<OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>>"
```

Assume we want only the inner delimiter `:` with the above result:

```js
let line = "if($<$<OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>>>)";
let result = getStringWithinDelimiters(line, line.indexOf("OR"), "$<", ">", false, ":", true);

// result now has "OR:$<CXX_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:Clang>"
```

### Simplified Example: Same Outer Delimiter and Optional Inner Delimiter

A simplified function with only one delimiter and optional inner delimiter is also provided.

Consider the following C++ code:

```cpp
#include <iostream>
#include <vector>

int main()
{
    std::vector<int> v = { 7, 8, 9 };

    std::cout << "Vector size: " << v.size() << std::endl();
    // ...
}
```

Assume we want `"Vector size: "`:

```js
let line = "std::cout << \"Vector size: \" << v.size() << std::endl();";
let del = "<<";
let result = getStringWithinSurroundDelimiter(line, line.indexOf("size:"), del);

// result now has " \"Vector size: \" "
```

Assume we want to get the `size()` function:

```js
let line = "std::cout << \"Vector size: \" << v.size() << std::endl();";
let del = "<<";
let result = getStringWithinSurroundDelimiter(line, line.indexOf(".") + 1, del, ".");

// result now has "size() "
```

Note that in any of these cases, if the input is invalid or there was nothing found, the return value will always be `null`.

## Contributing

PRs are welcome.

Create a branch against `master` and have the branch name be descriptive.

Before submitting, ensure that *all* tests pass and there are no linting errors.

## License

[MIT](LICENSE.txt)
