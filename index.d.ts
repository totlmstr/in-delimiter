// Type Definitions for in-delimiter

/**
 * Get string within delimiters.
 * @param {String} line input string
 * @param {Number} index zero-based position inside string
 * @param {String} openDel opening/first delimiter
 * @param {String} closeDel ending/last delimiter (optional)
 * @param {Boolean} withDel return string should have ALL input delimiter(s) (optional; default false)
 * @param {String} innerDel inner delimiter (optional)
 * @param {Boolean} withInnerDel return string should have ONLY INNER input delimiter (optional; default false)
 * @returns null if nothing was found or invalid input, otherwise a String
 */
export function getStringWithinDelimiters(line: string, index: number, openDel: string, closeDel?: string, withDel?: boolean, innerDel?: string, withInnerDel?: boolean): string | null;

/**
 * Get string within a surround delimiter.
 * @param {String} line input string
 * @param {Number} index zero-based position inside string
 * @param {String} del opening/closing delimiter
 * @param {Boolean} withDel return string should have ALL input delimiter(s) (optional; default false)
 * @param {String} innerDel inner delimiter (optional)
 * @param {Boolean} withInnerDel return string should have ONLY INNER input delimiter (optional; default false)
 * @returns null if nothing was found or invalid input, otherwise a String
 */
export function getStringWithinSurroundDelimiter(line: string, index: number, del: string, withDel?: string, innerDel?: string, withInnerDel?: boolean): string | null;
